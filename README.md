# hwm-frontend

This is the frontend for homework-manager.

## Get developing

Install Node.JS & Yarn and then...

```
git clone git@gitlab.com:homework-manager/hwm-frontend.git
cd hwm-frontend
yarn
yarn start
```

Done! server *should* be running @ localhost:1234

If you have any issues, delete `.cache` and `dist` folders and restart server.
