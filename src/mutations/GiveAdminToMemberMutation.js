import gql from 'graphql-tag'

export default gql`
  mutation giveAdminToMember(
    $groupId: ID!,
    $memberId: ID!,
  ) {
    giveAdminToMember(
      groupId: $groupId,
      memberId: $memberId,
    ) {
      _id
      members {
        user {
          _id
        }
        roles
      }
    }
  }
`
