import gql from 'graphql-tag'

export default gql`
  mutation createHomeworkTag(
    $name: String!,
    $color: HomeworkTagColor!,
    $groupId: ID!,
  ) {
    createHomeworkTag(
      name: $name,
      color: $color,
      groupId: $groupId,
    ) {
      # TODO: make this better;
      # we have to update this everytime GET_GROUP_QUERY
      # changes; that's not good.
      _id
      color
      name
    }
  }
`
