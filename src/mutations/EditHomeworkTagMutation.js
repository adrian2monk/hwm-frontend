import gql from 'graphql-tag'

export default gql`
  mutation editHomeworkTag(
    $_id: ID!,
    $name: String,
    $color: HomeworkTagColor,
  ) {
    editHomeworkTag(
      _id: $_id,
      name: $name,
      color: $color,
    ) {
      # TODO: make this better;
      # we have to update this everytime GET_GROUP_QUERY
      # changes; that's not good.
      _id
      name
      color
    }
  }
`
