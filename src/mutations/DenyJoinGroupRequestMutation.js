import gql from 'graphql-tag'

export default gql`
  mutation denyJoinGroupRequest(
    $requestId: ID!,
  ) {
    denyJoinGroupRequest(
      requestId: $requestId,
    )
  }
`
