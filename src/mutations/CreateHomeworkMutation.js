import gql from 'graphql-tag'

import GROUP_VIEW_HOMEWORK_FRAGMENT from '../fragments/GroupViewHomeworkFragment'

export default gql`
  mutation createHomework(
    $title: String!,
    $description: String,
    $groupId: ID!,
    $tagsIds: [ID!]!,
  ) {
    createHomework(
      title: $title,
      description: $description,
      groupId: $groupId,
      tagsIds: $tagsIds,
    ) {
      ...GroupViewHomework
    }
  }
  ${GROUP_VIEW_HOMEWORK_FRAGMENT}
`
