import gql from 'graphql-tag'

export default gql`
  mutation removeAdminFromMember(
    $groupId: ID!,
    $memberId: ID!,
  ) {
    removeAdminFromMember(
      groupId: $groupId,
      memberId: $memberId,
    ) {
      _id
      members {
        user {
          _id
        }
        roles
      }
    }
  }
`
