import gql from 'graphql-tag'

export default gql`
  mutation deleteHomework($homeworkId: ID!) {
    deleteHomework(_id: $homeworkId)
  }
`
