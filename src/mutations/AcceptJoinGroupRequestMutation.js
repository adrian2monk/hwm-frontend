import gql from 'graphql-tag'

export default gql`
  mutation acceptJoinGroupRequest(
    $requestId: ID!,
  ) {
    acceptJoinGroupRequest(
      requestId: $requestId,
    )
  }
`
