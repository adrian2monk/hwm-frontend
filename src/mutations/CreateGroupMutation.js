import gql from 'graphql-tag'

export default gql`
  mutation createGroup(
    $name: String!,
    $joinName: String!,
  ) {
    createGroup(
      name: $name,
      joinName: $joinName,
    ) {
      # TODO: make this better;
      # we have to update this everytime GET_GROUPS_QUERY
      # changes; that's not good.
      _id
      name
      joinName
    }
  }
`
