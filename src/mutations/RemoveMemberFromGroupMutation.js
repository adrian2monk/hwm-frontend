import gql from 'graphql-tag'

export default gql`
  mutation removeMemberFromGroup(
    $groupId: ID!,
    $memberId: ID!,
  ) {
    removeMemberFromGroup(
      groupId: $groupId,
      memberId: $memberId,
    ) {
      _id
    }
  }
`
