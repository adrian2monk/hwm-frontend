import gql from 'graphql-tag'

export default gql`
  mutation createAccount(
    $username: String!,
    $email: String!,
    $password: String!,
  ) {
    signup(
      username: $username,
      email: $email,
      password: $password,
    ) {
      token
      user {
        _id
        username
      }
    }
  }
`
