import gql from 'graphql-tag'

export default gql`
  mutation deleteHomeworkTag($homeworkTagId: ID!) {
    deleteHomeworkTag(_id: $homeworkTagId)
  }
`
