import gql from 'graphql-tag'

import GROUP_VIEW_HOMEWORK_FRAGMENT from '../fragments/GroupViewHomeworkFragment'

export default gql`
  mutation editHomework(
    $_id: ID!,
    $title: String,
    $description: String,
    $tagsIds: [ID!],
    $deadlineDate: String,
  ) {
    editHomework(
      _id: $_id,
      title: $title,
      description: $description,
      tagsIds: $tagsIds,
      deadlineDate: $deadlineDate,
    ) {
      ...GroupViewHomework
    }
  }
  ${GROUP_VIEW_HOMEWORK_FRAGMENT}
`
