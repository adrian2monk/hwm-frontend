import gql from 'graphql-tag'

export default gql`
  mutation requestJoinGroup(
    $groupId: ID,
    $groupJoinName: String,
  ) {
    requestJoinGroup(
      groupId: $groupId,
      groupJoinName: $groupJoinName,
    ) {
      _id
    }
  }
`
