import gql from 'graphql-tag'

import PROFILE_FRAGMENT from '../fragments/ProfileFragment'

export default gql`
  mutation editProfile(
    $username: String,
    $email: String,
  ) {
    editProfile(
      username: $username,
      email: $email,
    ) {
      ...Profile
    }
  }
  ${PROFILE_FRAGMENT}
`
