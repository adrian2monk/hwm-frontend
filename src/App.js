import React, { Component } from 'react'
import { ApolloProvider } from 'react-apollo'
import { Route, Switch } from 'react-router'
import { BrowserRouter } from 'react-router-dom'

import Nav from './components/Nav'

import Index from './views/Index'
import Login from './views/Login'
import CreateAccount from './views/CreateAccount'
import Profile from './views/Profile'
import LogOut from './views/LogOut'
import Groups from './views/Groups'
import Group from './views/Group'
import CreateGroup from './views/CreateGroup'
import RequestJoinGroup from './views/RequestJoinGroup'
import NotFound from './views/NotFound'

import apolloClient from './apollo'

// TODO: not show admin-only buttons to non-admin

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <ApolloProvider client={apolloClient}>
          <div>
            <Nav />
            <main>
              <Switch>
                <Route path="/" exact component={Index} />
                <Route path="/login" exact component={Login} />
                <Route path="/createAccount" exact component={CreateAccount} />
                <Route path="/profile" exact component={Profile} />
                <Route path="/logout" exact component={LogOut} />
                <Route path="/groups" exact component={Groups} />
                <Route path="/group/new" exact component={CreateGroup} />
                <Route path="/group/join" exact component={RequestJoinGroup} />
                <Route path="/group/:groupId" component={Group} />
                <Route component={NotFound} />
              </Switch>
            </main>
          </div>
        </ApolloProvider>
      </BrowserRouter>
    )
  }
}

export default App
