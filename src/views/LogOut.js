import React, { Component } from 'react'

import client from '../apollo'

class LogOut extends Component {
  state = {
    loading: false,
  }

  isLoggedIn () {
    return localStorage.getItem('jwtToken') ? true : false
  }

  async logOut () {
    this.setState({ loading: true })

    localStorage.setItem('jwtToken', undefined)

    // clear cache
    await client.resetStore().catch(error => {})

    this.setState({ loading: false })
  }

  componentWillMount () {
    this.logOut()
  }

  render () {
    return this.state.loading
      ? <h1>Logging out...</h1>
      : <h1>Successfully logged out.</h1>
  }
}

export default LogOut
