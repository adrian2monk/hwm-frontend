import React, { Component } from 'react'
import gql from 'graphql-tag'
import { Query, Mutation } from 'react-apollo'

import GET_PROFILE_QUERY from '../queries/GetProfileQuery'
import EDIT_PROFILE_MUTATION from '../mutations/EditProfileMutation'
import Error from '../components/Error'

const mapProfileToState = profile => ({
  profile: {
    username: profile.username,
    email: profile.email,
  },
})

class EditingProfile extends Component {
  state = { profile: null }

  static getDerivedStateFromProps (props, state) {
    if (!state.profile) {
      return { ...state, ...mapProfileToState(props.profile) }
    } else {
      return state
    }
  }

  changeProfileValue = event => {
    this.setState({
      profile: {
        ...this.state.profile,
        [event.target.name]: event.target.value,
      },
    })
  }

  onSubmit = editProfile => {
    return event => {
      event.preventDefault()

      const { profile } = this.state
      editProfile({
        variables: {
          username: profile.username,
          email: profile.email,
        },
      })

      return false
    }
  }

  render () {
    const { profile } = this.state
    return (
      <Mutation mutation={EDIT_PROFILE_MUTATION} update={this.props.update}>
        {(editProfile, { loading, error }) => {
          if (loading) return <h1>Editing profile...</h1>
          return (
            <form onSubmit={this.onSubmit(editProfile)}>
              {error && <Error error={error} />}
              <input type="text" name="username"
                value={profile.username} onChange={this.changeProfileValue} />
              <input type="email" name="email"
                value={profile.email} onChange={this.changeProfileValue} />
              <button>edit profile</button>
            </form>
          )
        }}
      </Mutation>
    )
  }
}

class Profile extends Component {
  state = { editing: false }

  toggleEditing = () => {
    this.setState({ editing: !this.state.editing })
  }

  finishEditing = () => {
    this.setState({ editing: false })
  }

  render () {
    return (
      <Query query={GET_PROFILE_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <h1>Loading...</h1>
          if (error) return <h1>Error! {error.message}</h1>

          const { profile } = data

          return (
            <div className="profile">
              <h1>Profile</h1>
              <button onClick={this.toggleEditing}>Edit profile</button>
              {this.state.editing
                ? <EditingProfile
                    profile={profile}
                    update={this.finishEditing}
                  />
                : <div>
                    <p>username: {profile.username}</p>
                    <p>email: {profile.email}</p>
                  </div>
              }
            </div>
          )
        }}
      </Query>
    )
  }
}

export default Profile
