import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import Error from '../components/Error'

import REQUEST_JOIN_GROUP_MUTATION from '../mutations/RequestJoinGroupMutation'

// TODO: make a list of already requested groups

class RequestJoinGroup extends Component {
  state = {
    joinName: '',
  }

  render () {
    return (
      <Mutation
        mutation={REQUEST_JOIN_GROUP_MUTATION}
        update={() => {
          this.setState({ joinName: '' })
        }}
      >
        {(requestJoinGroup, { loading, error, data }) => {
          console.log(data)
          return (
            <form onSubmit={event => {
              event.preventDefault()

              requestJoinGroup({
                variables: {
                  groupJoinName: this.state.joinName,
                },
              }).catch(error => {})

              return false
            }}>
              <h1>Request to join a group</h1>
              <Error error={error} />
              <input
                value={this.state.joinName}
                onChange={e => this.setState({ joinName: e.target.value })}
                type="text"
                placeholder="join name"
              />
              <button>Send request</button>
            </form>
          )
        }}
      </Mutation>
    )
  }
}

export default RequestJoinGroup
