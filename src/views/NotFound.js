import React from 'react'

const NotFound = () => (
  <div className="not-found">
    <h1>Not found</h1>
    <img src="https://http.cat/404" alt="funny cat" />
  </div>
)

export default NotFound
