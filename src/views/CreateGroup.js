import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import CREATE_GROUP_MUTATION from '../mutations/CreateGroupMutation'
import GET_GROUPS_QUERY from '../queries/GetGroupsQuery'

class CreateGroup extends Component {
  async onSubmit (event, createGroup) {
    event.preventDefault()

    const {
      name: { value: name },
      joinName: { value: joinName },
    } = event.target

    await createGroup({
      variables: { name, joinName },
    }).catch(e => {})

    return false
  }

  render () {
    return (
      <div className="create-group">
        <Mutation
          mutation={CREATE_GROUP_MUTATION}
          update={(cache, { data: { createGroup: groupResult } }) => {
            // We have to do a try-catch because it's possible that the user
            // didn't go to the groups view yet, and hence didn't fetch the groups,
            // which causes `cache.readQuery` to throw an error.
            // (when the user actually goes to that view, it's going to download
            // the required data.)
            try {
              const { profile } = cache.readQuery({
                query: GET_GROUPS_QUERY,
              })

              cache.writeQuery({
                query: GET_GROUPS_QUERY,
                data: {
                  profile: {
                    ...profile,
                    groups: [
                      ...profile.groups,
                      groupResult,
                    ],
                  },
                },
              })
            } catch (error) {
              // do nothing
            }
          }}
        >
          {(createGroup, { loading, error }) => {
            if (loading) return <h3>Creating group...</h3>

            return (
              // TODO: might want to do something else than passing
              // the createGroup function as an argument...
              <form onSubmit={e => this.onSubmit(e, createGroup)}>
                <h1>Create Group</h1>
                {error && <h3>Error! {error.message}</h3>}
                <input
                  type="text"
                  name="name"
                  placeholder="name"
                />
                <input
                  type="text"
                  name="joinName"
                  placeholder="joinName"
                />
                <button>Create group</button>
              </form>
            )
          }}
        </Mutation>
      </div>
    )
  }
}

export default CreateGroup
