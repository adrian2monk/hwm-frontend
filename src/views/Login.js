import React, { Component } from 'react'
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

import Error from '../components/Error'
import LOGIN_MUTATION from '../mutations/LoginMutation'
import GET_PROFILE_QUERY from '../queries/GetProfileQuery'

class Login extends Component {
  state = {
    username: '',
    password: '',
  }

  render () {
    return (
      <Mutation
        mutation={LOGIN_MUTATION}
        update={(cache, { data: { login } }) => {
          localStorage.setItem('jwtToken', login.token)
          // cache username to use it in the navbar
          let data = {}
          try {
            data = cache.readQuery({ query: GET_PROFILE_QUERY })
          } catch (error) {
            // ignore error; the user is probably not logged in
          }
          cache.writeQuery({
            query: GET_PROFILE_QUERY,
            data: { ...data, profile: { ...data.profile, ...login.user } },
          })
        }}
      >
        {(login, { loading, error }) => {
          if (loading) return <h1>Logging in...</h1>

          return (
            <form onSubmit={event => {
              event.preventDefault()

              login({
                variables: {
                  username: this.state.username,
                  password: this.state.password,
                },
              }).catch(error => {})

              return false
            }}>
              <h1>Login</h1>
              {error && <Error error={error} />}
              <input
                value={this.state.username}
                onChange={e => this.setState({ username: e.target.value })}
                type="text"
                placeholder="username"
              />
              <input
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value })}
                type="password"
                placeholder="password"
              />
              <button>log in</button>
            </form>
          )
        }}
      </Mutation>
    )
  }
}

export default Login
