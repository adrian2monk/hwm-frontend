import React, { Component } from 'react'
import { Query } from 'react-apollo'

import GroupHomeworks from './group/GroupHomeworks'
import GroupHomeworkTags from './group/GroupHomeworkTags'
import GroupJoinRequests from './group/GroupJoinRequests'
import GroupMembers from './group/GroupMembers'

import GET_GROUP_QUERY from '../queries/GetGroupQuery'

class Group extends Component {
  render () {
    const { groupId } = this.props.match.params

    return (
      <Query
        query={GET_GROUP_QUERY}
        variables={{ groupId }}
      >
        {({ loading, error, data, refetch }) => {
          if (loading) return <h1>Loading...</h1>
          if (error) return <h1>Error! {error.message}</h1>

          const { group, profile: { _id: userId } } = data

          return (
            <div>
              <h1>{group.name}</h1>
              <h2>Join name: {group.joinName}</h2>
              <button onClick={() => refetch()}>Refresh</button>
              <GroupHomeworkTags group={group} userId={userId} />
              <GroupHomeworks group={group} userId={userId} />
              <GroupJoinRequests group={group} userId={userId} />
              <GroupMembers group={group} userId={userId} />
            </div>
          )
        }}
      </Query>
    )
  }
}

export default Group
