import React, { Component } from 'react'
import { Query } from 'react-apollo'
import { Link } from 'react-router-dom'

import GET_GROUPS_QUERY from '../queries/GetGroupsQuery'

class Groups extends Component {
  render () {
    return (
      <Query query={GET_GROUPS_QUERY}>
        {({ loading, error, data, refetch }) => {
          if (loading) return <h1>Loading...</h1>
          if (error) return <h1>Error! {error.message}</h1>

          const groups = data.profile.groups

          return (
            <div>
              <h1>Groups</h1>
              <button onClick={() => refetch()}>refresh</button>
              <Link to="/group/new">Create Group</Link>
              |<Link to="/group/join">Request to join a group</Link>
              <ul>
                {groups.map(group => (
                  <li key={group._id}>
                    <Link to={`/group/${group._id}`}>
                      {group.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          )
        }}
      </Query>
    )
  }
}

export default Groups
