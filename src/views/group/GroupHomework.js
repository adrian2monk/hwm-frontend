import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import GET_GROUP_QUERY from '../../queries/GetGroupQuery'
import CREATE_HOMEWORK_MUTATION from '../../mutations/CreateHomeworkMutation'
import EDIT_HOMEWORK_MUTATION from '../../mutations/EditHomeworkMutation'
import { getMemberViaId, isMemberAdmin } from '../../utils'
import { getStringForDateInput } from '../../timeUtils'

import GroupHomeworkDeleteButton from './groupHomework/GroupHomeworkDeleteButton'

const mapHomeworkToState = homework => ({
  homework: {
    title: homework.title,
    description: homework.description,
    tagsIds: homework.tags.map(tag => tag._id),
    deadlineDate: homework.deadlineDate
      ? new Date(homework.deadlineDate)
      : undefined,
  },
})

class GroupHomework extends Component {
  state = {
    editing: false,
    homework: {
      title: '',
      description: '',
      tagsIds: [],
      deadlineDate: undefined,
    },
  }

  componentDidMount () {
    this.setHomework(this.props.homework)
  }

  updateField (field, value) {
    this.setState({
      homework: {
        ...this.state.homework,
        [field]: value,
      },
    })
  }

  setHomework (homework) {
    if (homework) {
      this.setState(
        mapHomeworkToState(homework)
      )
    }
  }

  toggleEditing () {
    this.setHomework(this.props.homework)
    this.setState({ editing: !this.state.editing })
  }

  static getDerivedStateFromProps (props, state) {
    if (!state.editing && props.homework) {
      return { ...state, ...mapHomeworkToState(props.homework) }
    } else if (props.createNew) {
      return { ...state, editing: true }
    } else {
      return null
    }
  }

  renderEditingHomework () {
    const { group: { _id: groupId }, createNew } = this.props

    return (
      <Mutation
        mutation={createNew
          ? CREATE_HOMEWORK_MUTATION
          : EDIT_HOMEWORK_MUTATION}
        update={(cache, { data: { createHomework } }) => {
          // It is not necessary to cache when editing as Apollo does that
          // for us automatically <3
          if (createNew) {
            const data = cache.readQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
            })

            cache.writeQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
              data: {
                ...data,
                group: {
                  ...data.group,
                  homeworks: [
                    ...data.group.homeworks,
                    createHomework,
                  ],
                },
              },
            })
          }
        }}
      >
        {(createOrEditHomework, { loading, error }) => {
          if (loading) {
            return <h3>
              {createNew ? 'creating' : 'editing'} homework...
            </h3>
          }

          const homework = this.state.homework

          return (
            <form onSubmit={async event => {
              event.preventDefault()

              await createOrEditHomework({
                variables: {
                  title: homework.title,
                  description: homework.description,
                  tagsIds: homework.tagsIds,
                  deadlineDate: homework.deadlineDate
                    && homework.deadlineDate.toGMTString(),
                  ...(createNew
                    ? { groupId }
                    : { _id: this.props.homework._id }
                  ),
                },
              })

              if (createNew) {
                if (this.props.homeworkCreatedCallback) {
                  this.props.homeworkCreatedCallback()
                }
              } else {
                this.setState({ editing: false })
              }

              return false
            }}>
              {error && <p>Error... {error.message}</p>}

              <input
                value={homework.title}
                onChange={e => this.updateField('title', e.target.value)}
                type="text"
                placeholder="title"
              />
              <input
                value={homework.description}
                onChange={e => this.updateField('description', e.target.value)}
                type="text"
                placeholder="description"
              />
              <input
                value={getStringForDateInput(homework.deadlineDate)}
                onChange={e => this.updateField('deadlineDate', new Date(e.target.value))}
                type="date"
                placeholder="deadline"
              />
              <ul>
                {// Sort tags to put the ones that are not in homework on top.
                // We have to do the [...array] because .sort() actually
                // mutates the array and React prevent us from mutating the
                // props (that's good!).
                [...this.props.group.homeworkTags].sort((tag1, tag2) =>
                  homework.tagsIds.indexOf(tag1._id) === -1 ? -1 : 1
                ).map(tag => {
                  const tagIsInHomework = homework.tagsIds.indexOf(tag._id) !== -1

                  return (
                    <li key={tag._id}>
                      {tag.name} [{tag.color}]
                      {tagIsInHomework
                        ? <span onClick={e => {
                            this.setState({
                              homework: {
                                ...homework,
                                tagsIds: homework.tagsIds.filter(tagId => (
                                  tagId !== tag._id
                                )),
                              }
                            })
                          }}>-</span>
                        : <span onClick={e => {
                            this.setState({
                              homework: {
                                ...homework,
                                tagsIds: [
                                  ...homework.tagsIds,
                                  tag._id,
                                ],
                              }
                            })
                          }}>+</span>
                      }
                    </li>
                  )
                })}
              </ul>
              <button>{createNew ? 'create' : 'edit'} homework</button>
            </form>
          )
        }}
      </Mutation>
    )
  }

  render () {
    const { editing } = this.state
    const { group, userId, homework } = this.props

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    if (editing) {
      return <div className="group-homework">
        {this.renderEditingHomework()}
      </div>
    }

    return (
      <div className="group-homework">
        <div>
          {homework.title}
          {homework.description
            ? <p>{homework.description}</p>
            : null
          }
          Deadline: {
            homework.deadlineDate
              ? new Date(homework.deadlineDate).toLocaleDateString()
              : 'not defined'
            }<br />
          Created at: {new Date(homework.createdAt).toLocaleString()}<br />
          Last edited at: {new Date(homework.updatedAt).toLocaleString()}<br />
          {homework.tags.map(tag => (
            <p key={tag._id}>[TAG] {tag.name} [{tag.color}]</p>
          ))}
          <button disabled={!isAdmin} onClick={e => {
            this.setState({ editing: true })
          }}>edit</button>
          <GroupHomeworkDeleteButton
            group={group}
            homework={homework}
            isAdmin={isAdmin}
          />
        </div>
      </div>
    )
  }
}

export default GroupHomework
