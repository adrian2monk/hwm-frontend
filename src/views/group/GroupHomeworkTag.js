import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import GET_GROUP_QUERY from '../../queries/GetGroupQuery'
import CREATE_HOMEWORK_TAG_MUTATION from '../../mutations/CreateHomeworkTagMutation'
import EDIT_HOMEWORK_TAG_MUTATION from '../../mutations/EditHomeworkTagMutation'
import DELETE_HOMEWORK_TAG_MUTATION from '../../mutations/DeleteHomeworkTagMutation'
import Error from '../../components/Error'
import { getMemberViaId, isMemberAdmin } from '../../utils'

const mapHomeworkTagToState = homeworkTag => ({
  tag: {
    name: homeworkTag.name,
    color: homeworkTag.color,
  },
})

class GroupHomeworkTag extends Component {
  state = {
    editing: false,
    tag: {
      name: '',
      color: '',
    },
  }

  componentDidMount () {
    this.setHomeworkTag(this.props.tag)
  }

  updateField (field, value) {
    this.setState({
      tag: {
        ...this.state.tag,
        [field]: value,
      },
    })
  }

  setHomeworkTag (homeworkTag) {
    if (homeworkTag) {
      this.setState(
        mapHomeworkTagToState(homeworkTag)
      )
    }
  }

  toggleEditing () {
    this.setHomework(this.props.tag)
    this.setState({ editing: !this.state.editing })
  }

  static getDerivedStateFromProps (props, state) {
    if (!state.editing && props.tag) {
      return { ...state, ...mapHomeworkTagToState(props.tag) }
    } else if (props.createNew) {
      return { ...state, editing: true }
    } else {
      return null
    }
  }

  renderEditingHomeworkTag () {
    const { group: { _id: groupId }, createNew } = this.props

    return (
      <Mutation
        mutation={createNew
          ? CREATE_HOMEWORK_TAG_MUTATION
          : EDIT_HOMEWORK_TAG_MUTATION}
        update={(cache, { data }) => {
          // It is not necessary to cache when editing as Apollo does that
          // for us automatically <3
          if (createNew) {
            const groupData = cache.readQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
            })

            cache.writeQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
              data: {
                ...groupData,
                group: {
                  ...groupData.group,
                  homeworkTags: [
                    ...groupData.group.homeworkTags,
                    data.createHomeworkTag,
                  ],
                },
              },
            })
          }
        }}
      >
        {(createOrEditHomeworkTag, { loading, error }) => {
          if (loading) {
            return <h3>
              {createNew ? 'creating' : 'editing'} homework tag...
            </h3>
          }

          return (
            <form onSubmit={async event => {
                event.preventDefault()

                await createOrEditHomeworkTag({
                  variables: {
                    name: this.state.tag.name,
                    color: this.state.tag.color,
                    ...(createNew
                      ? { groupId }
                      : { _id: this.props.tag._id }
                    ),
                  },
                })

                if (createNew) {
                  if (this.props.tagCreatedCallback) {
                    this.props.tagCreatedCallback()
                  }
                } else {
                  this.setState({ editing: false })
                }

                return false
              }}
            >
              {error && <p>Error... {error.message}</p>}

              <input
                value={this.state.tag.name}
                onChange={e => this.updateField('name', e.target.value)}
                type="text"
                placeholder="name"
              />
              <input
                value={this.state.tag.color}
                onChange={e => this.updateField('color', e.target.value)}
                type="text"
                placeholder="color"
              />
            <button>{createNew ? 'create' : 'edit'} homework tag</button>
            </form>
          )
        }}
      </Mutation>
    )
  }

  renderDeleteButton (props) {
    const { tag, group: { _id: groupId } } = this.props

    return (
      <Mutation
        mutation={DELETE_HOMEWORK_TAG_MUTATION}
        update={(cache) => {
          const data = cache.readQuery({
            query: GET_GROUP_QUERY,
            variables: { groupId },
          })

          cache.writeQuery({
            query: GET_GROUP_QUERY,
            variables: { groupId },
            data: {
              ...data,
              group: {
                ...data.group,
                // filter the homeworks and only leave the ones
                // that are NOT the same id as the homework just
                // deleted.
                homeworkTags: group.homeworkTags.filter(_homeworkTag => (
                  _homeworkTag._id !== tag._id
                )),
                // remove the tag from homeworks that has it
                homeworks: group.homeworks.map(homework => ({
                  ...homework,
                  tags: homework.tags.filter(_homeworkTag => (
                    _homeworkTag._id !== tag._id
                  )),
                })),
              },
            },
          })
        }}
      >
        {(deleteHomeworkTag, { loading, error }) => (
          <button {...props} onClick={() => deleteHomeworkTag({
            variables: {
              homeworkTagId: tag._id
            },
          })}>
            {error && <Error error={error} />}
            {loading
              ? 'deleting...'
              : 'delete'
            }
          </button>
        )}
      </Mutation>
    )
  }

  render () {
    const { editing } = this.state
    const { tag, group, userId } = this.props

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    if (editing && isAdmin) {
      return <div className="group-homework-tag">
        {this.renderEditingHomeworkTag()}
      </div>
    }

    return (
      <div className="group-homework-tag">
        <h4>{tag.name}</h4>
        <p>color: {tag.color}</p>
          <button disabled={!isAdmin} onClick={e => {
            this.setState({ editing: true })
          }}>edit</button>
          {this.renderDeleteButton({ disabled: !isAdmin })}
      </div>
    )
  }
}

export default GroupHomeworkTag
