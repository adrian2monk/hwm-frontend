import React, { Component } from 'react'

import GroupHomework from './GroupHomework'
import { getMemberViaId, isMemberAdmin } from '../../utils'

class GroupHomeworks extends Component {
  state = {
    creatingHomework: false,
  }

  render () {
    const { group, userId } = this.props

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    return (
      <div className="group-homeworks">
        <h3>Homeworks</h3>
        <button disabled={!isAdmin} onClick={e => {
          this.setState({ creatingHomework: !this.state.creatingHomework })
        }}>Create homework</button>
        <ul>
          {this.state.creatingHomework
            ? <li>
                <GroupHomework
                  createNew
                  homeworkCreatedCallback={() => {
                    this.setState({ creatingHomework: false })
                  }}
                  {...this.props}
                />
              </li>
            : null
          }
          {group.homeworks.map(homework => (
            <li key={homework._id}>
              <GroupHomework
                homework={homework}
                {...this.props}
              />
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default GroupHomeworks
