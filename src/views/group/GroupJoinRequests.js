import React, { Component } from 'react'

import GroupJoinRequest from './GroupJoinRequest'

class GroupJoinRequests extends Component {
  render () {
    const { group } = this.props

    return (
      <div className="group-join-requests">
        <h3>Join requests</h3>
        <ul>
          {group.joinRequests.map(request => (
            <li key={request._id}>
              <GroupJoinRequest
                request={request}
                {...this.props}
              />
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default GroupJoinRequests
