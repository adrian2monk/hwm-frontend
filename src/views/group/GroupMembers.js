import React, { Component } from 'react'

import GroupMember from './GroupMember'

class GroupMembers extends Component {
  render () {
    const { group } = this.props
    const { members } = group

    return (
      <div className="group-members">
        <h3>Members</h3>
        <ul>
          {members.map(member => (
            <li key={member.user._id}>
              <GroupMember
                member={member}
                {...this.props}
              />
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default GroupMembers
