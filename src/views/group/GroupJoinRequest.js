import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import ACCEPT_JOIN_GROUP_REQUEST_MUTATION
  from '../../mutations/AcceptJoinGroupRequestMutation'
import DENY_JOIN_GROUP_REQUEST_MUTATION
  from '../../mutations/DenyJoinGroupRequestMutation'
import GET_GROUP_QUERY from '../../queries/GetGroupQuery'
import Error from '../../components/Error'
import { getMemberViaId, isMemberAdmin } from '../../utils'

class GroupJoinRequest extends Component {
  render () {
    const { userId, group, request } = this.props

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    return (
      <div className="group-join-request">
        <h4>{request.userThatRequested.username}</h4>
        Requested at: {request.createdAt}
        <Mutation
          mutation={ACCEPT_JOIN_GROUP_REQUEST_MUTATION}
          update={(cache) => {
            const data = cache.readQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId: group._id },
            })

            cache.writeQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId: group._id },
              data: {
                ...data,
                group: {
                  ...data.group,
                  joinRequests: group.joinRequests.filter(_request => (
                    _request._id !== request._id
                  )),
                  members: [
                    ...group.members,
                    {
                      __typename: 'GroupMember',
                      user: request.userThatRequested,
                      roles: ['MEMBER'],
                    },
                  ],
                },
              },
            })
          }}
        >
          {(acceptRequest, { loading, error }) => {
            if (loading) return <span>Accepting request...</span>

            return (
              <span>
                {error && <Error error={error} />}
                <button disabled={!isAdmin} onClick={() => {
                  acceptRequest({ variables: { requestId: request._id } })
                }}>
                  Accept request
                </button>
              </span>
            )
          }}
        </Mutation>
        <Mutation
          mutation={DENY_JOIN_GROUP_REQUEST_MUTATION}
          update={(cache) => {
            const data = cache.readQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId: group._id },
            })

            cache.writeQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId: group._id },
              data: {
                ...data,
                group: {
                  ...data.group,
                  joinRequests: group.joinRequests.filter(_request => (
                    _request._id !== request._id
                  )),
                },
              },
            })
          }}
        >
          {(denyRequest, { loading, error }) => {
            if (loading) return <span>Denying request...</span>

            return (
              <span>
                {error && <Error error={error} />}
                <button disabled={!isAdmin} onClick={() => {
                  denyRequest({ variables: { requestId: request._id } })
                }}>
                  Deny request
                </button>
              </span>
            )
          }}
        </Mutation>
      </div>
    )
  }
}

export default GroupJoinRequest
