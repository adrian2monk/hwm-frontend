import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import GET_GROUP_QUERY from '../../queries/GetGroupQuery'
import REMOVE_MEMBER_FROM_GROUP_MUTATION
  from '../../mutations/RemoveMemberFromGroupMutation'
import GIVE_ADMIN_TO_MEMBER_MUTATION
  from '../../mutations/GiveAdminToMemberMutation'
import REMOVE_ADMIN_FROM_MEMBER_MUTATION
  from '../../mutations/RemoveAdminFromMemberMutation'
import Error from '../../components/Error'
import { getMemberViaId, isMemberAdmin } from '../../utils'

class GroupMember extends Component {
  render () {
    const { userId, member, group } = this.props
    const { _id: groupId } = group

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    const memberIsUser = member.user._id === userId

    const memberIsAdmin = isMemberAdmin(member)

    return (
      <div className="group-member">
        <h4>{member.user.username}</h4>
        Joined: {member.createdAt}<br />
        Roles: <ul>
          {member.roles.map((role, index) => (
            <li key={index}>{role}</li>
          ))}
        </ul>
        <Mutation
          mutation={REMOVE_MEMBER_FROM_GROUP_MUTATION}
          update={(cache) => {
            const data = cache.readQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
            })

            cache.writeQuery({
              query: GET_GROUP_QUERY,
              variables: { groupId },
              data: {
                ...data,
                group: {
                  ...data.group,
                  members: group.members.filter(_member => (
                    _member.user._id != member.user._id
                  )),
                },
              },
            })
          }}
        >
          {(removeMemberFromGroup, { loading, error }) => {
            if (loading) return <span>Removing member...</span>

            return (
              <span>
                {error && <Error error={error} />}
                <button
                  disabled={!isAdmin || memberIsUser}
                  onClick={() => removeMemberFromGroup({
                    variables: {
                      groupId,
                      memberId: member.user._id,
                    },
                  })}
                >remove member</button>
              </span>
            )
          }}
        </Mutation>
        <Mutation mutation={GIVE_ADMIN_TO_MEMBER_MUTATION}>
          {(giveAdminToMember, { loading, error }) => {
            if (loading) return <span>Giving admin...</span>

            return (
              <span>
                {error && <Error error={error} />}
                <button
                  disabled={!isAdmin || memberIsAdmin || memberIsUser}
                  onClick={() => giveAdminToMember({
                    variables: {
                      groupId,
                      memberId: member.user._id,
                    },
                  })}
                >give admin to member</button>
              </span>
            )
          }}
        </Mutation>
        <Mutation mutation={REMOVE_ADMIN_FROM_MEMBER_MUTATION}>
          {(removeAdminFromMember, { loading, error }) => {
            if (loading) return <span>Removing admin...</span>

            return (
              <span>
                {error && <Error error={error} />}
                <button
                  disabled={!isAdmin || !memberIsAdmin || memberIsUser}
                  onClick={() => removeAdminFromMember({
                    variables: {
                      groupId,
                      memberId: member.user._id,
                    },
                  })}
                >remove admin from member</button>
              </span>
            )
          }}
        </Mutation>
      </div>
    )
  }
}

export default GroupMember
