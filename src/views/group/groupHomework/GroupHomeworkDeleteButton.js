import React from 'react'
import { Mutation } from 'react-apollo'

import GET_GROUP_QUERY from '../../../queries/GetGroupQuery'
import DELETE_HOMEWORK_MUTATION from '../../../mutations/DeleteHomeworkMutation'

export default ({
  group: { _id: groupId },
  homework: { _id: homeworkId },
  isAdmin,
  ...props,
}) => (
  <Mutation
    mutation={DELETE_HOMEWORK_MUTATION}
    update={(cache) => {
      const data = cache.readQuery({
        query: GET_GROUP_QUERY,
        variables: { groupId },
      })

      cache.writeQuery({
        query: GET_GROUP_QUERY,
        variables: { groupId },
        data: {
          ...data,
          group: {
            ...data.group,
            homeworks: data.group.homeworks.filter(_homework => (
              _homework._id !== homeworkId
            )),
          },
        },
      })
    }}
  >
    {(deleteHomework, { loading, error }) => (
      <button {...props}
        disabled={!isAdmin || props.disabled}
        onClick={event => {
          deleteHomework({
            variables: { homeworkId },
          })
          if (props.onClick) props.onClick()
        }}
      >
        {error && error.message}
        {loading
          ? 'deleting...'
          : 'delete'
        }
      </button>
    )}
  </Mutation>
)
