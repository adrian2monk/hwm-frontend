import React, { Component } from 'react'

import GroupHomeworkTag from './GroupHomeworkTag'
import { getMemberViaId, isMemberAdmin } from '../../utils'

class GroupHomeworkTags extends Component {
  state = {
    creatingHomeworkTag: false,
  }

  render () {
    const { group, userId } = this.props

    const isAdmin =
      isMemberAdmin(getMemberViaId({ userId, members: group.members }))

    return (
      <div className="group-homework-tags">
        <h3>Homework tags</h3>
        <button disabled={!isAdmin} onClick={e => {
          this.setState({ creatingHomeworkTag: !this.state.creatingHomeworkTag })
        }}>Create homework tag</button>
        <ul>
          {this.state.creatingHomeworkTag
            ? <li>
                <GroupHomeworkTag
                  createNew
                  tagCreatedCallback={() => {
                    this.setState({ creatingHomeworkTag: false })
                  }}
                  {...this.props}
                />
              </li>
            : null
          }
          {group.homeworkTags.map(tag => (
            <li key={tag._id}>
              <GroupHomeworkTag
                tag={tag}
                {...this.props}
              />
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default GroupHomeworkTags
