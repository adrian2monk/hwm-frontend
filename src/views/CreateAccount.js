import React, { Component } from 'react'
import { Mutation } from 'react-apollo'

import CREATE_ACCOUNT_MUTATION from '../mutations/CreateAccountMutation'
import GET_PROFILE_QUERY from '../queries/GetProfileQuery'

class CreateAccount extends Component {
  state = {
    username: '',
    email: '',
    password: '',
  }

  render () {
    return (
      <Mutation
        mutation={CREATE_ACCOUNT_MUTATION}
        update={(cache, { data: { signup } }) => {
          localStorage.setItem('jwtToken', signup.token)
          // cache username to use it in the navbar
          let data = {}
          try {
            data = cache.readQuery({ query: GET_PROFILE_QUERY })
          } catch (error) {
            // ignore error; the user is probably not logged in
          }
          cache.writeQuery({
            query: GET_PROFILE_QUERY,
            data: { ...data, profile: { ...data.profile, ...signup.user } },
          })
        }}
      >
        {(createAccount, { loading, error }) => {
          if (loading) return <h1>Creating account...</h1>

          return (
            <form onSubmit={event => {
              event.preventDefault()

              createAccount({
                variables: {
                  username: this.state.username,
                  email: this.state.email,
                  password: this.state.password,
                },
              }).catch(error => {})

              return false
            }}>
              <h1>Create account</h1>
              {error && <p style={{color: 'red'}}>
                Error... :/ please try again
              </p>}
              {error && error.graphQLErrors &&
                error.graphQLErrors.map((error, index) => (
                  // TODO: i18n
                  <div key={index} style={{color: 'red'}}>
                    <p>
                      {error.name}
                      <small>{error.message}</small>
                    </p>
                  </div>
                ))
              }
              <input
                value={this.state.username}
                onChange={e => this.setState({ username: e.target.value })}
                type="text"
                placeholder="username"
              />
              <input
                value={this.state.email}
                onChange={e => this.setState({ email: e.target.value })}
                type="text"
                placeholder="email"
              />
              <input
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value })}
                type="password"
                placeholder="password"
              />
              <button>create account</button>
            </form>
          )
        }}
      </Mutation>
    )
  }
}

export default CreateAccount
