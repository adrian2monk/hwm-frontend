import gql from 'graphql-tag'

export default gql`
  fragment Profile on User {
    _id
    username
    email
  }
`
