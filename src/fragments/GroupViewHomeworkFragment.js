import gql from 'graphql-tag'

export default gql`
  fragment GroupViewHomework on Homework {
    _id
    title
    description
    tags {
      _id
      name
      color
    }
    createdAt
    updatedAt
    deadlineDate
  }
`
