import gql from 'graphql-tag'

export default gql`
  fragment GroupJoinRequest on GroupJoinRequest {
    _id
    userThatRequested {
      _id
      username
    }
    createdAt
  }
`
