import gql from 'graphql-tag'

export default gql`
  fragment GroupMember on GroupMember {
    user {
      _id
      username
    }
    roles
    createdAt
  }
`
