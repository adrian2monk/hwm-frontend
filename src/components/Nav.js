import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Query } from 'react-apollo'

import GET_PROFILE_QUERY from '../queries/GetProfileQuery'

class Nav extends Component {
  render () {
    return (
      <nav>
        <Link to="/">Home</Link>
        |<Link to="/login">Login</Link>
        |<Link to="/createAccount">create account</Link>
        |<Link to="/profile">Profile</Link>
        |<Link to="/groups">Groups</Link>
        <Query
          query={GET_PROFILE_QUERY}
          fetchPolicy="cache-first"
          errorPolicy="all"
        >
          {({ loading, error, data }) => {
            // We are ignoring errors here because errors persist even if the
            // cache is written over it, if we didn't ignore it, it would fail
            // even if it has the data
            if (loading || !data || !data.profile) return null

            const { profile } = data

            return (
              <span>
                |Welcome, {profile.username}
                |<Link to="/logout">log out</Link>
              </span>
            )
          }}
        </Query>
      </nav>
    )
  }
}

export default Nav
