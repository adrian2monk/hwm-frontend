import React from 'react'

const Error = ({ error }) => (
  <div
    className="error"
    style={{ color: 'red' }}
  >
    {error && <p>
      Error... :/ please try again
    </p>}
    {error && error.graphQLErrors &&
      error.graphQLErrors.map((error, index) => (
        // TODO: i18n
        <div key={index}>
          <p>
            {error.name}
            <small>{error.message}</small>
          </p>
        </div>
      ))
    }
  </div>
)

export default Error
