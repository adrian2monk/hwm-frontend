import gql from 'graphql-tag'

import GROUP_VIEW_HOMEWORK_FRAGMENT from '../fragments/GroupViewHomeworkFragment'
import GROUP_JOIN_REQUEST_FRAGMENT from '../fragments/GroupJoinRequestFragment'
import GROUP_MEMBER_FRAGMENT from '../fragments/GroupMemberFragment'

export default gql`
  query getGroup($joinName: String, $groupId: ID) {
    profile {
      _id
    }
    group(
      joinName: $joinName,
      _id: $groupId,
    ) {
      _id
      name,
      joinName
      homeworks {
        ...GroupViewHomework
      }
      homeworkTags {
        _id
        color
        name
      }
      members {
        ...GroupMember
      }
      joinRequests {
        ...GroupJoinRequest
      }
    }
  }
  ${GROUP_VIEW_HOMEWORK_FRAGMENT}
  ${GROUP_JOIN_REQUEST_FRAGMENT}
  ${GROUP_MEMBER_FRAGMENT}
`
