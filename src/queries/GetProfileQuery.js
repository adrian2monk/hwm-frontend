import gql from 'graphql-tag'

import PROFILE_FRAGMENT from '../fragments/ProfileFragment'

export default gql`
  query getProfile {
    profile {
      ...Profile
    }
  }
  ${PROFILE_FRAGMENT}
`
