import gql from 'graphql-tag'

export default gql`
  query getGroups {
    profile {
      _id
      groups {
        _id
        name
        joinName
      }
    }
  }
`
