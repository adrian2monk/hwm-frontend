import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import { InMemoryCache } from 'apollo-cache-inmemory'

export default new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      // if (graphQLErrors)
      //   graphQLErrors.map(({ message, locations, path }) =>
      //     console.log(
      //       `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
      //     )
      //   )
      if (networkError) console.log(`[Network error]: ${networkError}`)
    }),
    setContext((_, { headers }) => {
      const token = localStorage.getItem('jwtToken')
      return {
        headers: {
          ...headers,
          authorization: token ? `Bearer ${token}` : '',
        },
      }
    }),
    new HttpLink({
      uri: process.env.REACT_APP_API_URL || 'http://localhost:4000/',
      credentials: 'same-origin',
    }),
  ]),
  cache: new InMemoryCache({
    dataIdFromObject: object => object._id,
  }),
})
