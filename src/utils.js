export function getMemberViaId ({ userId, members }) {
  return members.find(member => member.user._id === userId)
}

export function isMemberAdmin (member) {
  return member.roles.indexOf('ADMIN') >= 0
}
